#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <openssl/sha.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL (clicked()), this, SLOT (on_pushButton_clicked()));
    connect(ui->pushButton_2, SIGNAL (clicked()), this, SLOT (on_pushButton_2_clicked()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString str = ui->lineEdit->text(); // Берем текст из строки
    unsigned char sha[20]; // Будущий шифр (20 битов - фиксированный размер SHA1 шифров)

    SHA1((unsigned char*)str.toStdString().c_str(), (size_t)str.length(), sha); // Хеширование

    char sha_result[20*2+1]; // Будущий итоговый хеш в виде цифр и букв

    for(int i = 0; i < 20; i++)
             sprintf(&sha_result[i*2], "%02x", (unsigned int)sha[i]); // Перекодируем каждый символ в хеше

    ui->label->setText(sha_result); // Выводим строчку хеша
}

void MainWindow::on_pushButton_2_clicked()
{
    QPixmap map;
    map.load(":/new/prefix1/2.png");
    ui->label_3->setPixmap(map);
}
